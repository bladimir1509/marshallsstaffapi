using AutoMapper;
using FluentValidation.AspNetCore;
using Jugadores.Core.Interfaces;
using MarshallsStaff.Core.Interfaces;
using MarshallsStaff.Core.Services;
using MarshallsStaff.Infrastructure.Data;
using MarshallsStaff.Infrastructure.Filters;
using MarshallsStaff.Infrastructure.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarshallsStaff.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            // Para ignorar la dependencia ciclica
            services.AddControllers(options =>
            {
                options.Filters.Add<GlobalExceptionFilter>();

            }).AddNewtonsoftJson(options =>

                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            ).ConfigureApiBehaviorOptions(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddDbContext<MarshallsContext>(options =>
           options.UseSqlServer(Configuration.GetConnectionString("MarshallsDB"))
   );
            services.AddRazorPages();
            ConfigureRepositoriesServices(services);
            services.AddCors();
            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(ValidationFilter));

            }).AddFluentValidation(options =>
            {
                options.RegisterValidatorsFromAssemblies(AppDomain.CurrentDomain.GetAssemblies());
            });
            AddSwagger(services);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }
//            app.UseCors(
//   options => options.WithOrigins("http://localhost:4200").AllowAnyMethod()
//);
            app.UseCors(builder => builder
     .AllowAnyOrigin()
     .AllowAnyMethod()
     .AllowAnyHeader());

            //app.UseMvc();


            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();


            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "MarshallsStaff API V1");
            });
        }
        private void AddSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                var groupName = "v1";

                options.SwaggerDoc(groupName, new OpenApiInfo
                {
                    Title = $"MarshallsStaff {groupName}",
                    Version = groupName,
                    Description = "MarshallsStaff API",
                    Contact = new OpenApiContact
                    {
                        Name = "MarshallsStaff",
                        Email = string.Empty,
                        Url = new Uri("https://Marshalls.com/"),
                    }
                });
            });
        }
        public void ConfigureRepositoriesServices(IServiceCollection services)
        {

            // Services
            services.AddTransient<ICatalogService, CatalogService>();
            services.AddTransient<IStaffSalaryService, StaffSalaryService>();

            // Repositories

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IStaffSalaryRepository, StaffSalaryRepository>();

            services.AddScoped(typeof(IRepository<>), typeof(BaseRepository<>));

        }

 
    }
}
