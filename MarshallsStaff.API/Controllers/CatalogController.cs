﻿using AutoMapper;
using MarshallsStaff.API.Responses;
using MarshallsStaff.Core.DTOs;
using MarshallsStaff.Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarshallsStaff.API.Controllers
{
    [Route("api/catalogs")]
    [ApiController]
    public class CatalogController : ControllerBase
    {
        private readonly ICatalogService _catalogService;
        private readonly IMapper _mapper;

        public CatalogController(ICatalogService catalogService, IMapper mapper)
        {
            _catalogService = catalogService;
            _mapper = mapper;
        }

        [HttpGet("positions")]
        public IActionResult GetPositions()
        {
            var positions = _catalogService.GetPositions();

            var positionsDto = _mapper.Map<IEnumerable<CatalogDto>>(positions);

            var response = new ApiResponse<IEnumerable<CatalogDto>>(positionsDto);

            return Ok(response);

        }
        [HttpGet("divisions")]
        public IActionResult GetDivisions()
        {
            var divisions = _catalogService.GetDivisions();

            var divisionsDto = _mapper.Map<IEnumerable<CatalogDto>>(divisions);

            var response = new ApiResponse<IEnumerable<CatalogDto>>(divisionsDto);

            return Ok(response);

        }
        [HttpGet("offices")]
        public IActionResult GetOffices()
        {
            var offices = _catalogService.GetOffices();

            var officesDto = _mapper.Map<IEnumerable<CatalogDto>>(offices);

            var response = new ApiResponse<IEnumerable<CatalogDto>>(officesDto);

            return Ok(response);

        }
    }
}
