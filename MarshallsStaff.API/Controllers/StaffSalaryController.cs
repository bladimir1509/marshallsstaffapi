﻿using AutoMapper;
using MarshallsStaff.API.Responses;
using MarshallsStaff.Core.Data;
using MarshallsStaff.Core.DTOs;
using MarshallsStaff.Core.DTOs.Inputs;
using MarshallsStaff.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MarshallsStaff.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StaffSalaryController : ControllerBase
    {
        private readonly IStaffSalaryService _staffSalaryService;
        private readonly IMapper _mapper;
        public StaffSalaryController(IMapper mapper, IStaffSalaryService staffSalaryService )
        {
            _mapper = mapper;
            _staffSalaryService = staffSalaryService;

        }
        // GET: api/staffSalary
        [HttpGet]
        public IActionResult Get()
        {
            var staffSalaries = _staffSalaryService.GetStaffSalaries();

            var staffSalariesDto = _mapper.Map<IEnumerable<StaffSalariesDto>>(staffSalaries);

            var response = new ApiResponse<IEnumerable<StaffSalariesDto>>(staffSalariesDto);

            return Ok(response);
        }

        // POST api/<StaffSalaryController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] StaffSalariesDto staffSalariesDto )
        {
            var staffSalary = _mapper.Map<StaffSalary>(staffSalariesDto);

            await _staffSalaryService.InsertStaffSalary(staffSalary);

            staffSalariesDto = _mapper.Map<StaffSalariesDto>(staffSalary);

            var response = new ApiResponse<StaffSalariesDto>(staffSalariesDto);

            return Ok(response);
        }

        // PUT api/<StaffSalaryController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] StaffSalariesDto staffSalariesDto)
        {
            var staffSalary = _mapper.Map<StaffSalary>(staffSalariesDto);
            staffSalary.Id = id;

            var result = await _staffSalaryService.UpdateStaffSalary(staffSalary);

            var response = new ApiResponse<bool>(result);

            return Ok(response);
        }

        // DELETE api/<StaffSalaryController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _staffSalaryService.DeleteStaffSalary(id);

            var response = new ApiResponse<bool>(result);

            return Ok(response);
        }
    }
}
