﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarshallsStaff.Core.Entities
{

   public  class TableRequest
    {
        public int? PageSize { get; set; }
        public int? Page { get; set; }
    }
}
