﻿using MarshallsStaff.Core.Entities;
using System;
using System.Collections.Generic;

#nullable disable

namespace MarshallsStaff.Core.Data
{
    public partial class Division: BaseEntity
    {
        public Division()
        {
            StaffSalaries = new HashSet<StaffSalary>();
        }

        public string Name { get; set; }

        public virtual ICollection<StaffSalary> StaffSalaries { get; set; }
    }
}
