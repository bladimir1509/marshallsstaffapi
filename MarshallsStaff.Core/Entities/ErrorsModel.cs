﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarshallsStaff.Core.Entities
{
    public class ErrorsModel
    {
        public string Property { get; set; }
        public IEnumerable<string> Error { get; set; }
    }
}
