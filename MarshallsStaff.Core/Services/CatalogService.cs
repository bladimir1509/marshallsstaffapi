﻿using Jugadores.Core.Interfaces;
using MarshallsStaff.Core.Data;
using MarshallsStaff.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarshallsStaff.Core.Services
{
    public class CatalogService : ICatalogService
    {
        private readonly IUnitOfWork _unitOfWork;
        public CatalogService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public IEnumerable<Position> GetPositions()
        {
            return _unitOfWork.PositionRepository.GetAll();

        }
        public IEnumerable<Division> GetDivisions()
        {
            return _unitOfWork.DivisionRepository.GetAll();

        }
        public IEnumerable<Office> GetOffices()
        {
            return _unitOfWork.OfficeRepository.GetAll();

        }

    }
}
