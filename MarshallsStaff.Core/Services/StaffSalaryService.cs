﻿using Jugadores.Core.Interfaces;
using MarshallsStaff.Core.Data;
using MarshallsStaff.Core.DTOs.Inputs;
using MarshallsStaff.Core.Exceptions;
using MarshallsStaff.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace MarshallsStaff.Core.Services
{
    public class StaffSalaryService : IStaffSalaryService
    {
        private readonly IUnitOfWork _unitOfWork;
        public StaffSalaryService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public IEnumerable<StaffSalary> GetStaffSalaries()
        {
            return _unitOfWork.StaffSalaryRepository.GetAll();

        }

        public async Task InsertStaffSalary(StaffSalary staff)
        {
            List<StaffSalary> existStaff =  _unitOfWork.StaffSalaryRepository.GetAll().ToList();
            if (existStaff.Exists(x => x.EmployeeName == staff.EmployeeName && x.EmployeeSurname == staff.EmployeeSurname )) 
                throw new BusinessException("Ya existe un empleado con ese nombre");
            
            if (existStaff.Exists(x => x.EmployeeCode == staff.EmployeeCode))
                throw new BusinessException("Ya existe un empleado con ese código");

            if (existStaff.Exists(x => x.IdentificationNumber == staff.IdentificationNumber))
                throw new BusinessException("Ya existe un empleado con ese número de identificación");


            await _unitOfWork.StaffSalaryRepository.Add(staff);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<bool> UpdateStaffSalary(StaffSalary staff)
        {
            _unitOfWork.StaffSalaryRepository.Update(staff);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }
        public async Task<bool> DeleteStaffSalary(int id)
        {
            await _unitOfWork.StaffSalaryRepository.Delete(id);
            await _unitOfWork.SaveChangesAsync();

            return true;
        }


    }
}
