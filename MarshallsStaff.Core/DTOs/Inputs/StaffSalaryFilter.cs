﻿using MarshallsStaff.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarshallsStaff.Core.DTOs.Inputs
{
    public class StaffSalaryFilter : TableRequest
    {
        public string EmployeeCode { get; set; }
        public int Division { get; set; }
        public int Position { get; set; }
        public int Grade { get; set; }
        public int Office { get; set; }
    }
}
