﻿using MarshallsStaff.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarshallsStaff.Core.DTOs
{
    public class CatalogDto: BaseEntity
    {
        public string Name { get; set; }
    }
}
