﻿using MarshallsStaff.Core.Data;
using System.Collections.Generic;

namespace MarshallsStaff.Core.Interfaces
{
    public interface ICatalogService
    {
        IEnumerable<Position> GetPositions();
        IEnumerable<Division> GetDivisions();
        IEnumerable<Office> GetOffices();
    }
}