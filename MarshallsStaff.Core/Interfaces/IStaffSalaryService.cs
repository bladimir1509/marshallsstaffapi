﻿using MarshallsStaff.Core.Data;
using MarshallsStaff.Core.DTOs.Inputs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MarshallsStaff.Core.Interfaces
{
    public interface IStaffSalaryService
    {
        Task<bool> DeleteStaffSalary(int id);
        IEnumerable<StaffSalary> GetStaffSalaries();
        Task InsertStaffSalary(StaffSalary staff);
        Task<bool> UpdateStaffSalary(StaffSalary staff);
    }
}