﻿using MarshallsStaff.Core.Data;
using MarshallsStaff.Core.DTOs.Inputs;
using System.Collections.Generic;

namespace MarshallsStaff.Core.Interfaces
{
    public interface IStaffSalaryRepository : IRepository<StaffSalary>
    {
        IEnumerable<StaffSalary> GetStaffSalariesFilter(StaffSalaryFilter filter);
    }
}