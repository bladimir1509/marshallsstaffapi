﻿using MarshallsStaff.Core.Data;
using MarshallsStaff.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Jugadores.Core.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Division> DivisionRepository { get; }
        IRepository<Office> OfficeRepository { get; }
        IRepository<Position> PositionRepository { get; }
        IStaffSalaryRepository StaffSalaryRepository { get; }
        void SaveChanges();  
        Task SaveChangesAsync();
    }
}
