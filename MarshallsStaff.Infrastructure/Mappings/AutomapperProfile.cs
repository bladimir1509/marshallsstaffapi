﻿using AutoMapper;
using MarshallsStaff.Core.Data;
using MarshallsStaff.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarshallsStaff.Infrastructure.Mappings
{
    public class AutomapperProfile : Profile
    {

        public AutomapperProfile()
        {
            CreateMap<Position, CatalogDto>();
            CreateMap<Office, CatalogDto>();
            CreateMap<Division, CatalogDto>();
            CreateMap<StaffSalary, StaffSalariesDto>().ReverseMap();
            //CreateMap<StaffSalariesDto, StaffSalary>();

        }
    }
}
