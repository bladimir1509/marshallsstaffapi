﻿using Jugadores.Core.Interfaces;
using MarshallsStaff.Core.Data;
using MarshallsStaff.Core.Interfaces;
using MarshallsStaff.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MarshallsStaff.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        public readonly MarshallsContext _context;
        private readonly IRepository<Position> _positionRepository;
        private readonly IRepository<Office> _officeRepository;
        private readonly IRepository<Division> _divisionRepository;
        private readonly IStaffSalaryRepository _staffSalaryRepository;

        public UnitOfWork(MarshallsContext context)
        {
            _context = context;
        }
        public IRepository<Position> PositionRepository => _positionRepository ?? new BaseRepository<Position>(_context);
        public IRepository<Office> OfficeRepository => _officeRepository ?? new BaseRepository<Office>(_context);
        public IRepository<Division> DivisionRepository => _divisionRepository ?? new BaseRepository<Division>(_context);
        public IStaffSalaryRepository StaffSalaryRepository => _staffSalaryRepository ?? new StaffSalaryRepository(_context);


        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
