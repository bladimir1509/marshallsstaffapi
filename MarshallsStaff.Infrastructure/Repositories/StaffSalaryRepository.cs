﻿using MarshallsStaff.Core.Data;
using MarshallsStaff.Core.DTOs.Inputs;
using MarshallsStaff.Core.Interfaces;
using MarshallsStaff.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarshallsStaff.Infrastructure.Repositories
{
    public class StaffSalaryRepository : BaseRepository<StaffSalary>, IStaffSalaryRepository
    {
        public StaffSalaryRepository(MarshallsContext context) : base(context)
        {

        }

        public IEnumerable<StaffSalary> GetStaffSalariesFilter(StaffSalaryFilter filter)
        {
            var query = _entities.Where(x =>
            ((filter.EmployeeCode == null) || x.EmployeeCode.Contains(filter.EmployeeCode))
            && (filter.Office == 0 || x.Office == filter.Office)
            && (filter.Position == 0 || x.Position == filter.Position)
            && (filter.Grade == 0 || x.Grade == filter.Grade)
             );

            if (filter.Page != null && filter.PageSize != null)
            {
                return query = query.Skip((int)((filter.Page - 1) * filter.PageSize)).Take((int)filter.PageSize);
            }
            return query;
        }
    }
}
