﻿using System;
using MarshallsStaff.Core.Data;
using MarshallsStaff.Infrastructure.Data.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace MarshallsStaff.Infrastructure.Data
{
    public partial class MarshallsContext : DbContext
    {
        public MarshallsContext()
        {
        }

        public MarshallsContext(DbContextOptions<MarshallsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Division> Divisions { get; set; }
        public virtual DbSet<Office> Offices { get; set; }
        public virtual DbSet<Position> Positions { get; set; }
        public virtual DbSet<StaffSalary> StaffSalaries { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");
            modelBuilder.ApplyConfiguration(new PositionConfiguration());
            modelBuilder.ApplyConfiguration(new OfficeConfiguration());
            modelBuilder.ApplyConfiguration(new DivisionConfiguration());
            modelBuilder.ApplyConfiguration(new StaffSalaryConfiguration());

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
