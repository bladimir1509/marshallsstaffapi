﻿using MarshallsStaff.Core.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarshallsStaff.Infrastructure.Data.Configurations
{
    public class PositionConfiguration : IEntityTypeConfiguration<Position>
    {
        public void Configure(EntityTypeBuilder<Position> builder)
        {
            builder.HasKey(e => e.Id);

            builder.ToTable("Position");

            builder.Property(e => e.Id)
                   .HasColumnName("Id");

            builder.Property(e => e.Name)
                  .HasColumnName("Name")
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

        }
    }
    }
