﻿using MarshallsStaff.Core.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarshallsStaff.Infrastructure.Data.Configurations
{
   public class DivisionConfiguration : IEntityTypeConfiguration<Division>
    {
        public void Configure(EntityTypeBuilder<Division> builder)
        {
            builder.HasKey(e => e.Id);

           builder.ToTable("Division");

            builder.Property(e => e.Id)
                   .HasColumnName("Id");

            builder.Property(e => e.Name)
                  .HasColumnName("Name")
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
         
        }
        }
}
