﻿using MarshallsStaff.Core.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarshallsStaff.Infrastructure.Data.Configurations
{
    public class OfficeConfiguration : IEntityTypeConfiguration<Office>
    {
        public void Configure(EntityTypeBuilder<Office> builder)
        {
            builder.HasKey(e => e.Id);

            builder.ToTable("Office");
            builder.Property(e => e.Id)
                      .HasColumnName("Id");

            builder.Property(e => e.Name)
                  .HasColumnName("Name")
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false);



        }
    }
}
