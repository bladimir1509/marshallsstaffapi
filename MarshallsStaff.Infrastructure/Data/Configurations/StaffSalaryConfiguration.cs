﻿using MarshallsStaff.Core.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarshallsStaff.Infrastructure.Data.Configurations
{
    public class StaffSalaryConfiguration : IEntityTypeConfiguration<StaffSalary>
    {
        public void Configure(EntityTypeBuilder<StaffSalary> builder)
        {
            builder.HasKey(e => e.Id);

            builder.ToTable("StaffSalaries");

            builder.Property(e => e.BaseSalary).HasColumnType("decimal(18, 2)");

                builder.Property(e => e.BeginDate).HasColumnType("date");

                builder.Property(e => e.Birthday).HasColumnType("date");

                builder.Property(e => e.Commission).HasColumnType("decimal(18, 2)");

                builder.Property(e => e.CompensationBonus).HasColumnType("decimal(18, 2)");

                builder.Property(e => e.Contributions).HasColumnType("decimal(18, 2)");

                builder.Property(e => e.EmployeeCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                builder.Property(e => e.EmployeeName)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                builder.Property(e => e.EmployeeSurname)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                builder.Property(e => e.IdentificationNumber)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                builder.Property(e => e.ProductionBonus).HasColumnType("decimal(18, 2)");

                builder.HasOne(d => d.DivisionNavigation)
                    .WithMany(p => p.StaffSalaries)
                    .HasForeignKey(d => d.Division)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StaffSalaries_Division");

                builder.HasOne(d => d.OfficeNavigation)
                    .WithMany(p => p.StaffSalaries)
                    .HasForeignKey(d => d.Office)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StaffSalaries_Office");

                builder.HasOne(d => d.PositionNavigation)
                    .WithMany(p => p.StaffSalaries)
                    .HasForeignKey(d => d.Position)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StaffSalaries_Position");
        

        }
    }
}
